package id3

import (
	"fmt"
	"io"

	"bitbucket.org/flat20/id3/syncbytes"
)

const headerSize int = 10

// A Header holds the ID3 tag header information.
type Header struct {
	Version, Revision byte
	Flags             byte
	Unsynchronization bool
	Compression       bool
	Experimental      bool
	ExtendedHeader    bool
	Footer            bool   // id3v2.4.0
	ID3TagSize        uint32 // Tag size Excludes this header, includes extended header
}

// ReadHeader reads and parses a header from the Reader
func ReadHeader(reader io.Reader) (*Header, error) {

	data := make([]byte, headerSize)
	n, err := io.ReadFull(reader, data)
	if n < headerSize || err != nil || string(data[:3]) != "ID3" {
		return nil, fmt.Errorf("No ID3v2 header. %q", err)
	}

	size := syncbytes.ToUint32(data[6:])

	header := &Header{
		Version:    data[3],
		Revision:   data[4],
		Flags:      data[5],
		ID3TagSize: size,
	}

	hasBit := func(flag, index byte) bool {
		return flag&(1<<index) != 0
	}

	switch header.Version {
	case 2:
		header.Unsynchronization = hasBit(header.Flags, 7)
		header.Compression = hasBit(header.Flags, 6)
	case 3:
		header.Unsynchronization = hasBit(header.Flags, 7)
		header.ExtendedHeader = hasBit(header.Flags, 6)
		header.Experimental = hasBit(header.Flags, 5)
	case 4:
		header.Unsynchronization = hasBit(header.Flags, 7)
		header.ExtendedHeader = hasBit(header.Flags, 6)
		header.Experimental = hasBit(header.Flags, 5)
		header.Footer = hasBit(header.Flags, 4)
		//log.Printf("ID3v4 !! try it %d %t", header.Version, header.footer)
	default:
		return nil, fmt.Errorf("Unknown ID3 version: %d", header.Version)
	}

	//log.Printf("Version: 2.%d.%d Size: %d", header.Version, header.revision, size)
	//log.Printf("Header: %+v", header)

	return header, nil
}

// WriteHeader encodes the Header into the Writer.
func WriteHeader(fo io.Writer, h *Header) error {

	data := make([]byte, 0, headerSize)
	data = append(data, []byte("ID3")...)
	data = append(data, h.Version, h.Revision, h.Flags)
	// But flags should be created from header data?
	data = append(data, syncbytes.FromUint32(h.ID3TagSize)...)

	_, err := fo.Write(data)
	if err != nil {
		return err
	}

	return nil

}
