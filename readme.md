# README #

A quick ID3 tag reader & writer. It's faster than most decoders because it
doesn't automatically attempt to decode each frame within the ID3 tag. Instead
the raw []byte data for each frame is stored which can later be decoded.

### TODO ###

Rename ReadID3Tag/WriteID3Tag to Decode/Encode
