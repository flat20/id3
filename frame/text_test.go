package frame

import (
	"reflect"
	"strings"
	"testing"
)

// Frame text contents for testing
// Encoding 1 UTF-16 Little Endian, Text: "Jubilee & Burt Fox"
var ftBytes = []byte{1, 255, 254, 74, 0, 117, 0, 98, 0, 105, 0, 108, 0, 101, 0, 101, 0, 32, 0, 38, 0, 32, 0, 66, 0, 117, 0, 114, 0, 116, 0, 32, 0, 70, 0, 111, 0, 120, 0, 00, 00}
var ftString = "Jubilee & Burt Fox"

func TestRunes(t *testing.T) {
	enc := ftBytes[0]
	runes := DecodeRunes(enc, ftBytes[1:])
	//runes = stripTerm(runes)

	if strings.Compare(ftString, string(runes)) != 0 {
		t.Errorf("%+v isn't %v.", ftString, string(runes))
	}

	// Append terminator and encode
	bytes, err := EncodeRunes(enc, runes)
	if err != nil {
		t.Error(err)
	}

	// Prepend the encoding
	bytes = append([]byte{enc}, bytes...)

	if !reflect.DeepEqual(ftBytes, bytes) {
		t.Errorf("Not equal %v %v", ftBytes, bytes)
	}

}

func TestUTF8(t *testing.T) {
	r := []rune("a世界ö")
	b := encUTF8(r)

	res := decUTF8(b)

	if !reflect.DeepEqual(r, res) {
		t.Errorf("Not equal %v %v", r, res)
	}
}

/*
func TestStripNull(t *testing.T) {
	r := []rune("a世界ö")
	log.Printf("%+v", r)

	r = stripTerm(r)
	log.Printf("%+v", r)
}*/

func TestTextFrame(t *testing.T) {
	tf := []byte{1, 255, 254, 86, 0, 97, 0, 114, 0, 105, 0, 111, 0, 117, 0, 115, 0, 32, 0, 65, 0, 114, 0, 116, 0, 105, 0, 115, 0, 116, 0, 115, 0, 0, 0}
	f, err := DecodeTextFrame(tf)
	if err != nil {
		t.Error(err)
	}

	if f.Encoding != 1 {
		t.Errorf("Encoding was %d not 1", f.Encoding)
	}
	if strings.Compare(f.Text, "Various Artists") != 0 {
		t.Errorf("Text was %s not Various Artists", f.Text)
	}

	b, err := EncodeTextFrame(f)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(tf, b) {
		t.Errorf("Encode not equal to original %v %v", tf, b)
	}
}

func TestCommentFrame(t *testing.T) {
	// encoding 3 and no short or full comment?
	//comm := []byte{3, 80, 117, 114, 99, 104, 97, 115, 101, 100, 32, 97, 116, 32, 66, 101, 97, 116, 112, 111, 114, 116, 46, 99, 111, 109}
	comm := []byte{1, 101, 110, 103, 255, 254, 0, 0, 255, 254, 49, 0, 57, 0, 57, 0, 52, 0, 104, 0, 105, 0, 112, 0, 104, 0, 111, 0, 112, 0, 46, 0, 99, 0, 111, 0, 109, 0, 0, 0}
	f, err := DecodeCommentFrame(comm)
	if err != nil {
		t.Error(err)
	}

	if f.Encoding != 1 {
		t.Errorf("Encoding was %d not 1", f.Encoding)
	}
	if strings.Compare(f.Full, "1994hiphop.com") != 0 {
		t.Errorf("Full was %s not 1994hiphop.com", f.Full)
	}

	if len(f.Short) != 0 {
		t.Errorf("Short was: %s. Should have been empty", f.Short)
	}

	b, err := EncodeCommentFrame(f)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(comm, b) {
		t.Errorf("Encode not equal to original %v %v", comm, b)
	}

}

func TestFindStrEnd(t *testing.T) {
	b := []byte{255, 254, 49, 0, 57, 0, 57, 0, 52, 0, 104, 0, 105, 0, 112, 0, 104, 0, 111, 0, 112, 0, 46, 0, 99, 0, 111, 0, 109, 0, 0, 0}
	var enc byte = 1

	n := findStrEnd(enc, b)
	if n != len(b) {
		t.Errorf("n was %d not %d", n, len(b))
	}

	b = []byte{255, 254, 49, 0, 57, 0, 57, 0, 52, 0, 104, 0, 105, 0, 112, 0, 104, 0, 111, 0, 112, 0, 46, 0, 99, 0, 111, 0, 109, 10, 0, 0}
	n = findStrEnd(enc, b)
	if n != len(b) {
		t.Errorf("n was %d not %d", n, len(b))
	}

}

var ff *TextFrame

func BenchmarkDecodeTextFrame(b *testing.B) {

	tf := []byte{1, 255, 254, 86, 0, 97, 0, 114, 0, 105, 0, 111, 0, 117, 0, 115, 0, 32, 0, 65, 0, 114, 0, 116, 0, 105, 0, 115, 0, 116, 0, 115, 0, 0, 0}

	for n := 0; n < b.N; n++ {
		f, err := DecodeTextFrame(tf)
		if err != nil {
			b.Error(err)
		}
		ff = f
	}
}
