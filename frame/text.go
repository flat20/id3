package frame

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"unicode/utf16"
)

// TODO Rename to TextEncoding

// EncType is the type the text is encoded with
type EncType byte

// EncTypes supported
const (
	EncTypeISO88591 EncType = 0
	EncTypeUTF16    EncType = 1
	EncTypeUTF16BE  EncType = 2
	EncTypeUTF8     EncType = 3
)

//var ErrInvalidEncoding = errors.New("Invalid Encoding Type")

// TextFrame is encoded text
type TextFrame struct {
	Encoding byte
	Text     string
}

// DecodeTextFrame parse a TextFrame using the correct encoding.
func DecodeTextFrame(data []byte) (*TextFrame, error) {

	f := &TextFrame{}

	f.Encoding = data[0]

	runes := DecodeRunes(f.Encoding, data[1:])
	f.Text = string(runes)

	return f, nil
}

// EncodeTextFrame generates a []byte
func EncodeTextFrame(f *TextFrame) ([]byte, error) {
	data := make([]byte, 1)
	data[0] = f.Encoding

	sb, err := EncodeRunes(f.Encoding, []rune(f.Text))
	if err != nil {
		return nil, err
	}

	data = append(data, sb...)
	return data, nil
}

// CommentFrame description
type CommentFrame struct {
	Encoding byte
	Language [3]byte
	Short    string
	Full     string
}

// NewCommentFrame sets up a CommentFrame ready to be encoded.
func NewCommentFrame(e EncType, lang string, short string, full string) *CommentFrame {
	var l [3]byte
	copy(l[:], []byte(lang))

	return &CommentFrame{Encoding: byte(e), Language: l, Short: short, Full: full}
}

// DecodeCommentFrame parse a CommentFrame
func DecodeCommentFrame(data []byte) (*CommentFrame, error) {

	f := &CommentFrame{}

	f.Encoding = data[0]
	copy(f.Language[:], data[1:4])

	// Skip to 'short' string.
	data = data[4:]

	// No terminator for the 'short' comment, which means it's
	// probably missing. This is incorrect but we'll let it slide as it happens a lot.
	n := findStrEnd(f.Encoding, data)
	if n == -1 {
		f.Short = ""
		n = 0
	} else {
		//
		runes := DecodeRunes(f.Encoding, data[:n])
		f.Short = string(runes)
	}

	// Read 'full' string from the remaining data
	runes := DecodeRunes(f.Encoding, data[n:])
	f.Full = string(runes)

	return f, nil
}

// EncodeCommentFrame generates a []byte
func EncodeCommentFrame(f *CommentFrame) ([]byte, error) {
	data := make([]byte, 4)
	data[0] = f.Encoding
	copy(data[1:4], f.Language[:])

	// Encode 'short' to []byte
	sb, err := EncodeRunes(f.Encoding, []rune(f.Short))
	if err != nil {
		return nil, err
	}
	data = append(data, sb...)

	// Encode 'full' to []byte
	fb, err := EncodeRunes(f.Encoding, []rune(f.Full))
	if err != nil {
		return nil, err
	}

	data = append(data, fb...)
	return data, nil
}

// DecodeRunes parses a slice of runes from all of the data, using the supplied
// encoding.
func DecodeRunes(encoding byte, data []byte) (runes []rune) {
	switch EncType(encoding) {
	case EncTypeISO88591:

		for _, c := range data {
			runes = append(runes, rune(c))
		}
	case EncTypeUTF16: // BOM

		runes = decUTF16BOM(data)

	case EncTypeUTF16BE: // No BOM, big endian
		runes = decUTF16(data, binary.BigEndian)
	case EncTypeUTF8:
		runes = decUTF8(data)

	}

	runes = stripTerm(runes)

	return
}

// Remove trailing null runes.
func stripTerm(t []rune) []rune {
	if len(t) == 0 {
		return t
	}

	i := len(t) - 1
	for ; i >= 0 && t[i] == 0; i-- {
		t = t[:i]
	}
	return t
}

// EncodeRunes into a []byte. Appends the null terminator.
func EncodeRunes(encoding byte, text []rune) (data []byte, err error) {

	// Append null
	text = append(text, 0)

	switch EncType(encoding) {
	case EncTypeISO88591: // Terminated with $00
		for _, r := range text {
			data = append(data, byte(r))
		}
	case EncTypeUTF16: // UTF-16 with BOM Terminated with $00 00
		b, err := encUTF16(text, binary.LittleEndian)
		if err != nil {
			return data, err
		}
		data = append(data, b...)
	case EncTypeUTF16BE: // without BOM Terminated with $00 00.
		b, err := encUTF16(text, binary.BigEndian)
		if err != nil {
			return data, err
		}
		data = append(data, b...)
	case EncTypeUTF8: // Terminated with $00.
		b := encUTF8(text)
		data = append(data, b...)
	}
	return
}

// Unicode conversion
func decUTF8(data []byte) []rune {
	return []rune(string(data))
}

// Returns the length of by including the terminating null. -1 if no null found
func findStrEnd(encoding byte, b []byte) int {

	switch EncType(encoding) {
	case EncTypeISO88591, EncTypeUTF8: // Terminated with $00
		return bytes.IndexByte(b, 0)

	case EncTypeUTF16, EncTypeUTF16BE: // Terminated with $00 00
		n := len(b) - 1
		for i := 0; i < n; i += 2 {
			if b[i] == 0 && b[i+1] == 0 {
				return i + 2
			}
		}

	}
	return -1
}

func decUTF16BOM(data []byte) []rune {

	bo := decBom(data)
	if bo == nil {
		return nil
	}

	return decUTF16(data[2:], bo)
}

// TODO decUTF16E and pass endian.
/*
func decUTF16BE(data []byte) []rune {
	u16s := byteToUint16(data, binary.BigEndian)
	return utf16.Decode(u16s)
}*/
func decUTF16(data []byte, bo binary.ByteOrder) []rune {
	u16s := byteToUint16(data, bo)
	return utf16.Decode(u16s)

}

func byteToUint16(data []byte, bo binary.ByteOrder) []uint16 {

	// Using bo.Uint16(data[i : i+2]) is cleaner but 4 times slower.

	var a, b int = 0, 1
	if bo == binary.BigEndian {
		a, b = b, a
	}

	n := len(data)
	u16s := make([]uint16, 0, n/2)
	for i := 0; i < n; i += 2 {
		u16 := uint16(data[i+a]) | uint16(data[i+b])<<8
		u16s = append(u16s, u16)
	}
	return u16s
}

func encUTF16(text []rune, bo binary.ByteOrder) ([]byte, error) {

	u16s := utf16.Encode(text)
	b := make([]byte, len(u16s)*2+2) // len + bom

	err := encBom(b, bo)
	if err != nil {
		return nil, err
	}

	for i := range u16s {
		bo.PutUint16(b[2+i*2:], u16s[i])
	}

	return b, nil
}

func encUTF16BE(text []rune) ([]byte, error) {

	u16s := utf16.Encode(text)
	b := make([]byte, len(u16s)*2)

	for i := range u16s {
		binary.BigEndian.PutUint16(b[i*2:], u16s[i])
	}

	return b, nil
}

// Encode to UTF-8
func encUTF8(text []rune) []byte {
	return []byte(string(text))
}

var bomLE = []byte{0xff, 0xfe}
var bomBE = []byte{0xfe, 0xff}

// Reads bom from the first 2 bytes of the slice. Returns nil if unknown byte order.
func decBom(b []byte) binary.ByteOrder {
	if b[0] == bomLE[0] && b[1] == bomLE[1] {
		return binary.LittleEndian
	}
	if b[0] == bomBE[0] && b[1] == bomBE[1] {
		return binary.BigEndian
	}
	return nil
}

// Sets bom in first 2 bytes of the slice.
func encBom(b []byte, bo binary.ByteOrder) error {
	switch bo {
	case binary.LittleEndian:
		b[0], b[1] = bomLE[0], bomLE[1]
		return nil
	case binary.BigEndian:
		b[0], b[1] = bomBE[0], bomBE[1]
		return nil
	default:
		return fmt.Errorf("Unknown byte order")
	}

}
