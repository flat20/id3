package frame

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"bitbucket.org/flat20/id3/syncbytes"
)

// Header sizes
const (
	v22FrameHeaderSize = 6
	v23FrameHeaderSize = 10
)

// Frame contains the raw []byte data which once Decoded are stored in the
// struct fields.
type Frame struct {
	ID          string
	Size        uint32
	StatusFlags byte
	FormatFlags byte
	Header      []byte // Raw header
	Data        []byte // Raw frame data
}

// Decoder decodes one Frame from the io.Reader
type Decoder interface {
	Decode() (*Frame, error) // Return EOF when no more frames can be read
	Len() int
}

// ErrFrameEmpty means that the frame had a size of less than 0.
var ErrFrameEmpty = errors.New("Frame was empty")

// NewDecoder returns a decoder suitable for the ID3 version set in Header.
// framesSize is the total size of the frames (ID3TagSize - ID3TagHeaderSize)
func NewDecoder(r io.Reader, version byte, framesSize uint32) Decoder {

	// TODO this could be done externally
	lr := &io.LimitedReader{R: r, N: int64(framesSize)}

	switch version {
	case 2:
		return &v22Decoder{reader: lr}
	case 3, 4:
		return &v23Decoder{reader: lr}
	default:
		return nil
	}
}

// v22Decoder decodes one v2.2 Frame from the io.Reader
type v22Decoder struct {
	reader *io.LimitedReader
}

// Decode one v2.2 Frame from the io.Reader. Returns EOF when no more
// frames are available.
func (fd *v22Decoder) Decode() (*Frame, error) {

	f := &Frame{}

	f.Header = make([]byte, v22FrameHeaderSize)

	if n, err := io.ReadFull(fd.reader, f.Header); n != v22FrameHeaderSize || err != nil {
		return nil, fmt.Errorf("couldn't read frame header. %q", err)
	}

	f.ID = string(f.Header[:3])

	size := binary.BigEndian.Uint32(f.Header[3:6])

	if size < 1 {
		return f, ErrFrameEmpty
	}

	f.Size = size

	// Would make more sense if we split up Frame and FrameHeader perhaps?
	f.Data = make([]byte, f.Size)
	if n, err := io.ReadFull(fd.reader, f.Data); n != int(f.Size) || err != nil {
		return f, fmt.Errorf("Couldn't read frame data %q", err)
	}

	return f, nil
}

// Len returns the number of bytes remaining of the ID3 in the reader.
func (fd *v22Decoder) Len() int {
	return int(fd.reader.N)
}

// v23Decoder decodes one v2.3+ Frame from the io.Reader
type v23Decoder struct {
	reader *io.LimitedReader
}

// Decode one v2.3+ Frame from the io.Reader. Returns EOF when no more
// frames are available.
func (fd *v23Decoder) Decode() (*Frame, error) {

	f := &Frame{}

	f.Header = make([]byte, v23FrameHeaderSize)
	if n, err := io.ReadFull(fd.reader, f.Header); n != v23FrameHeaderSize || err != nil {
		return nil, fmt.Errorf("couldn't read frame header. %q", err)
	}

	f.ID = string(f.Header[:4])

	size := syncbytes.ToUint32(f.Header[4:8])

	if size < 1 {
		return f, ErrFrameEmpty
	}

	f.Size = size

	// Flags
	f.StatusFlags = f.Header[8]
	f.FormatFlags = f.Header[9]

	// Would make more sense if we split up Frame and FrameHeader perhaps?
	f.Data = make([]byte, f.Size)
	if n, err := io.ReadFull(fd.reader, f.Data); n != int(f.Size) || err != nil {
		return f, fmt.Errorf("Couldn't read frame data %q", err)
	}

	return f, nil
}

// Len returns the number of bytes remaining of the ID3 in the reader.
func (fd *v23Decoder) Len() int {
	return int(fd.reader.N) //bytesLeft
}

// Encoder encodes the Frame properties into its Header []byte and updates
// the .Size field
type Encoder interface {
	Encode(f *Frame) error
}

// NewEncoder returns an encoder suitable for the ID3 version
func NewEncoder(version byte) Encoder {
	switch version {
	case 2:
		return &v22Encoder{}
	case 3, 4:
		return &v23Encoder{}
	default:
		return nil
	}

}

// v22Encoder encodes v2.2 Frames
type v22Encoder struct {
}

// Encode the Frame into the Header field
func (e *v22Encoder) Encode(f *Frame) error {

	f.Header = make([]byte, v22FrameHeaderSize)

	f.Size = uint32(len(f.Data))

	copy(f.Header[:3], []byte(f.ID))
	binary.BigEndian.PutUint32(f.Header[3:6], f.Size)
	return nil

}

// v23Encoder encodes v2.3+ Frames
type v23Encoder struct {
}

// Encode the Frame into the Header field
func (e *v23Encoder) Encode(f *Frame) error {

	f.Header = make([]byte, v23FrameHeaderSize)

	f.Size = uint32(len(f.Data))

	copy(f.Header[:4], []byte(f.ID))

	binary.BigEndian.PutUint32(f.Header[4:8], f.Size)

	// Flags
	f.Header[8] = f.StatusFlags
	f.Header[9] = f.FormatFlags

	return nil

}
