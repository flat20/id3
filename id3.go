// Package id3 contains a set of tools to read & write the ID3 v2 tag from
// audio files. It supports version 2.2, 2.3 & 2.4.
package id3

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"

	"bitbucket.org/flat20/id3/frame"
)

// ID3Tag consists of a Header and a series of Frames.
type ID3Tag struct {
	Header *Header
	Frames []*frame.Frame
}

// CalcSize updates the Header ID3TagSize based on the Data and Header fields
// of all frames. The ID3TagSize needs to be updated in case any Frame is
// changed and re-encoded.
func (t *ID3Tag) CalcSize() {
	t.Header.ID3TagSize = CalcSize(t.Frames)
}

// CalcSize accumulates all Frame.Data and Frame.Header sizes
func CalcSize(fs []*frame.Frame) (id3TagSize uint32) {

	for _, f := range fs {
		id3TagSize += uint32(len(f.Data) + len(f.Header))
	}
	return

}

// Decode parses a header and frames from the start of the Reader.
// The reader is moved to the end of the ID3Tag after the frames have been
// decoded.
func Decode(r io.Reader) (*ID3Tag, error) {

	header, err := ReadHeader(r)
	if err != nil {
		return nil, err
	}

	fs, err := ReadFrames(header, r)
	if err != nil && err != frame.ErrFrameEmpty {
		return nil, err
	}

	return &ID3Tag{header, fs}, nil
}

// Encode updates the ID3TagSize and writes the Header followed
// by the frames to the writer.
func Encode(w io.Writer, t *ID3Tag) error {

	t.CalcSize() // Calculate new size

	err := WriteHeader(w, t.Header)
	if err != nil {
		return err
	}

	err = WriteFrames(w, t.Frames)
	if err != nil {
		return err
	}

	return nil
}

// ReadFrames decodes all frames from the Reader. Forwards the reader to the end
// of the tag unless EOF is reached.
func ReadFrames(h *Header, r io.Reader) ([]*frame.Frame, error) {

	// We've already read the 10 byte header
	fd := frame.NewDecoder(r, h.Version, h.ID3TagSize-uint32(headerSize))
	if fd == nil {
		return nil, fmt.Errorf("not good")
	}

	frames := make([]*frame.Frame, 0, 10) // Initial capacity at 10

	for {
		f, err := fd.Decode()
		if err != nil {
			log.Printf("Notice - Stopped parsing frames. %d bytes left. %q", fd.Len(), err)

			if err == io.EOF { // Not an error
				return frames, nil
			}

			// Read until end of ID3Tag so following functions are positioned
			// well.
			io.CopyN(ioutil.Discard, r, int64(fd.Len()))

			return frames, err
		}

		frames = append(frames, f)
	}

}

// WriteFrames writes all frames to the Writer
func WriteFrames(w io.Writer, frames []*frame.Frame) error {

	for _, f := range frames {

		_, err := w.Write(f.Header)
		if err != nil {
			return err
		}
		_, err = w.Write(f.Data)
		if err != nil {
			return err
		}

	}

	return nil
}
