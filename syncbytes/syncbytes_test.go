package syncbytes

import (
	"reflect"
	"testing"
)

var synch = []struct {
	synched   []byte
	unsynched uint32
}{
	{[]byte{0x44, 0x7a, 0x70, 0x04}, 144619524},
	{[]byte{0, 4, 51, 116}, 72180},
	{[]byte{0, 0, 47, 51}, 6067},
	{[]byte{0, 5, 46, 63}, 87871},
}

func TestUnsynch(t *testing.T) {

	for _, tt := range synch {
		if result := ToUint32(tt.synched); result != tt.unsynched {
			t.Errorf("ToUint32 %d = %d want %d", tt.synched, result, tt.unsynched)
		}
	}

}

func TestUnsynch2(t *testing.T) {

	for _, tt := range synch {
		if result := syncintDecode(tt.synched); result != tt.unsynched {
			t.Errorf("syncintDecode %d = %d want %d", tt.synched, result, tt.unsynched)
		}
	}

}

// From .c code Only used for verifying that we get the same result
func syncintDecode(value []byte) uint32 {
	var a, b, c, d, result uint32

	a = uint32(value[3])
	b = uint32(value[2])
	c = uint32(value[1])
	d = uint32(value[0])

	/*
		a = value & 0xFF
		b = (value >> 8) & 0xFF
		c = (value >> 16) & 0xFF
		d = (value >> 24) & 0xFF
	*/
	result = result | a
	result = result | (b << 7)
	result = result | (c << 14)
	result = result | (d << 21)

	return result
}

func TestSynch(t *testing.T) {

	for _, tt := range synch {
		result := FromUint32(tt.unsynched)
		if !reflect.DeepEqual(result, tt.synched) {
			t.Errorf("FromUint32 %d = %d want %d", tt.unsynched, result, tt.synched)
		}
	}

}
