package syncbytes

// ToUint32 unsynchronizes the slice to a uint32
func ToUint32(bytes []byte) (result uint32) {

	//
	for _, b := range bytes {
		result = (result << 7) | uint32(b)
	}

	return
}

// FromUint32 ..
func FromUint32(n uint32) []byte {
	mask := uint32(1<<7 - 1)
	bytes := make([]byte, 4)

	for i := range bytes {
		bytes[len(bytes)-i-1] = byte(n & mask)
		n >>= 7
	}

	return bytes
}
