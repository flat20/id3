package id3

import (
	"bytes"
	"io"
	"log"
	"os"
	"testing"

	"bitbucket.org/flat20/id3/frame"
)

const filename = "v4.mp3"

// Read everything after the ID3 header.
func TestID3Strip(t *testing.T) {

	fi, err := os.OpenFile(filename, os.O_RDWR, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fi.Close()

	stat, err := fi.Stat()
	if err != nil {
		t.Fatal(err)
	}

	tag, err := Decode(fi)
	if err != nil {
		t.Fatal(err)
	}

	mlen := stat.Size() - int64(tag.Header.ID3TagSize)

	var music bytes.Buffer
	n, err := io.Copy(&music, fi)
	if err != nil {
		t.Fatalf("Unable to copy remainder of ID3 tag. %q", err)
	}

	if mlen != n {
		t.Fatalf("Should have copied %d bytes, was %d", mlen, n)
	}

	// Drop last 128 bytes if they contain an old ID3 v1 tag.
	var r io.Reader
	r = &music
	if string(music.Bytes()[n-128:n-125]) == "TAG" {
		r = io.LimitReader(&music, int64(n-128))
	}

	// Write mp3 without header.
	fo, err := os.OpenFile("out.mp3", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fo.Close()

	_, err = io.Copy(fo, r)
	if err != nil {
		t.Fatal(err)
	}

}

func TestDecode(t *testing.T) {

	fi, err := os.OpenFile(filename, os.O_RDWR, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fi.Close()

	tag, err := Decode(fi)
	if err != nil {
		t.Fatal(err)
	}

	for _, f := range tag.Frames {
		if f.ID == "TIT2" || f.ID == "TPE1" || f.ID == "TPE2" || f.ID == "TKEY" {
			tf, err := frame.DecodeTextFrame(f.Data)
			if err != nil {
				t.Error(err)
			}
			log.Printf("text frame: %s = (%d) %v", f.ID, tf.Encoding, tf.Text)
		}
		if f.ID == "COMM" {
			tf, err := frame.DecodeCommentFrame(f.Data)
			if err != nil {
				t.Error(err)
			}
			log.Printf("text frame: %s = (%d) %+v, %+v", f.ID, tf.Encoding, tf.Short, tf.Full)
		}
	}

}

func TestEncode(t *testing.T) {

	fi, err := os.OpenFile(filename, os.O_RDWR, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fi.Close()

	stat, err := fi.Stat()
	if err != nil {
		t.Fatal(err)
	}

	tag, err := Decode(fi)
	if err != nil {
		t.Fatal(err)
	}

	rl := stat.Size() - int64(tag.Header.ID3TagSize)

	var music bytes.Buffer
	n, err := io.Copy(&music, fi)
	if err != nil {
		t.Fatalf("Unable to copy remainder of ID3 tag. %q", err)
	}

	if n != rl {
		t.Fatalf("Didn't copy %d music bytes. Copied: %d", n, rl)
	}

	// Keep only TIT2

	var keep []*frame.Frame
	for _, f := range tag.Frames {
		if f.ID == "TIT2" {
			keep = append(keep, f)
		}
	}

	// Read only music, could be faster to just seek the original header.size

	/*
		music, err := stripHeader(fi)
		if err != nil {
			t.Fatal(err)
		}
	*/
	// Create out file

	fo, err := os.OpenFile("out.mp3", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fo.Close()

	tag.Frames = keep

	err = Encode(fo, tag)
	if err != nil {
		t.Fatal(err)
	}

	mlen := music.Len()
	wn, err := io.Copy(fo, &music)
	if err != nil {
		t.Fatal(err)
	}

	if int(wn) != mlen {
		t.Fatalf("Wrote %d not %d bytes of music", wn, mlen)
	}

	//log.Printf("Wrote %d bytes of music", nn)

}

func TestID3Modify(t *testing.T) {

	fi, err := os.OpenFile(filename, os.O_RDWR, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fi.Close()

	tag, err := Decode(fi)
	if err != nil {
		t.Fatal(err)
	}

	var music bytes.Buffer
	_, err = io.Copy(&music, fi)
	if err != nil {
		t.Fatalf("Unable to copy remainder of ID3 tag. %q", err)
	}
	/*
		music, err := readAudio(fi, tag.Header)
		if err != nil {
			t.Fatal(err)
		}
	*/
	// Modify comment and keep it

	fenc := frame.NewEncoder(tag.Header.Version)

	var keep []*frame.Frame
	for _, f := range tag.Frames {
		if f.ID == "COMM" {
			cf, e := frame.DecodeCommentFrame(f.Data)
			if e != nil {
				t.Fatal(e)
			}

			//cf.Short = "My short"
			cf.Full = "My full comment"
			f.Data, e = frame.EncodeCommentFrame(cf)
			if e != nil {
				t.Fatal(e)
			}

			e = fenc.Encode(f)
			if e != nil {
				t.Fatal(e)
			}
			//			encodeFrameHeader(header, f)
			//binary.BigEndian.PutUint32(f.Header[4:8], f.Size)

			//size := binary.BigEndian.Uint32(header[4:8])

			//log.Printf("%+v", f)
			keep = append(keep, f)
		}
	}

	// Create out file

	fo, err := os.OpenFile("out.mp3", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		t.Fatal(err)
	}
	defer fo.Close()

	tag.Frames = keep

	err = Encode(fo, tag)
	if err != nil {
		t.Fatal(err)
	}

	mlen := music.Len()
	wn, err := io.Copy(fo, &music)
	if err != nil {
		t.Fatal(err)
	}

	if int(wn) != mlen {
		t.Fatalf("Wrote %d not %d bytes of music", wn, mlen)
	}

}

/*
func readAudio(fi *os.File, header *Header) (*bytes.Buffer, error) {

	// Read the audio portion
	_, err := fi.Seek(int64(header.ID3TagSize), io.SeekStart)
	if err != nil {
		return nil, err
	}

	stat, err := fi.Stat()
	if err != nil {
		return nil, err
	}

	outSize := stat.Size() - int64(header.ID3TagSize)
	music := bytes.NewBuffer(make([]byte, 0, outSize))
	n, err := io.Copy(music, fi)
	if err != nil {
		return music, err
	}
	if n != outSize {
		return music, fmt.Errorf("Didn't copy all the music. %d != %d bytes", n, outSize)
	}
	return music, nil
}
*/
